(function() {

    var UserRegApp = angular.module("UserReg", []);

    var UserRegCtrl = function() {
        var vm = this;

        vm.User = {
            name: "",
            email: "",
            password: "",
            gender: "",
            genderDesc: ["Male", "Female"],
            dateOfBirth: "",
            address: "",
            country: "",
            contact_number: ""
        };

        vm.signup = signup;
        vm.isAgeValid = isAgeValid;
        vm.isCharValid = isCharValid;

        function signup() {
            console.info("register click");
            console.info("name: %s", vm.User.name);
            console.info("email: %s", vm.User.email);
            console.info("password: %s", vm.User.password);
            console.info("gender: %s", vm.User.gender);
            console.info("dateOfBirth: %s", vm.User.dateOfBirth);
            console.info("address: %s", vm.User.address);
            console.info("country: %s", vm.User.country);
            console.info("contact_number: %s", vm.User.contact_number);
        }

        function isAgeValid() {
            var date = new Date(vm.User.dateOfBirth);
            date.setFullYear(date.getFullYear() + 18);
            return date < new Date();
        }

        function isCharValid() {
            var result = /[^0-9\\+\\-\\(\\)]/g(vm.User.contact_number);
            return !result;
        }

    UserRegApp.controller("UserRegCtrl", UserRegCtrl);

    }
})();
