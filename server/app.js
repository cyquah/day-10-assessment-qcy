var express = require("express");

var app = express();

app.use(express.static(__dirname + "/../client/"));

app.get("/", function (req, res) {
    var data = req.params;
    console.log(data);
    res.send("Submission successful, Thank You");
});

app.use(function (req, res) {
    res.redirect("/page-not-found.html");
});

app.use(function (err, req, res, next) {
    res.redirect("/501.html");
});

app.listen(4000, function() {
    console.info("Web server started on port 4000");
});
